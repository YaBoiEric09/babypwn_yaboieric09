# OSUSEC CTF League Babypwn Write Up

This Challenge we were given the babypwn Binary and a server that ran the `nc chal.ctf-league.osusec.org 4747`

Loading the file in Ghidra we can see a few things. First there is a `user_input()` function and a `print_flag()` function. 

Looking at the user_input() 
We can see that there is a fgets that reads the user input into the address of local_18. With fgets we are able to exploit this and cause a buffer overflow. 

`
fgets((char *)&local_18,200,stdin);
`

From there we need to know the stack inorder to hijack the flow of the binary to be able to return to the print_flag() and run that function. 

We can then do this by opening up ghidra and we can then see when we click on the function user_input(). We can see the arguments that are present on the stack. We can see that local_18 is -0x18 on the stack. which is 24 in decimal so the stack will look like this


Return Addr

Saved RBP

RBP - 8

local_10

local_18

Now we can overwrite the stack until from local_18 which is our user input until the Return Address. Since this is an x64 bit binary each level in the stack is 8 bytes. So we need to fill the buffer with 24 bytes. 

Once the buffer is full we can then overwrite the Return Address, which should be the address of `print_flag()`. We can get this address in Ghidra as well, `0x400687`. 

With that we can create a python script to connect to the server, hijack the workflow of the binary, and display the flag.

```python
from pwn import * 

p = remote("chal.ctf-league.osusec.org", 4747)

flag_addr = p64(0x400687)
buf = b"A" * 24

p.sendline(buf + flag_addr)

p.interactive()
```

Resulting in the Flag:
`osu{c0ngr4tz_on_F1r5T_pwn}`
